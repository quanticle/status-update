# Status Update App

This is an app designed to help track standup statuses

## Dev Env Setup Instructions
1. Create a folder for this app: example `~\status_update`
2. Create a Python3 virtualenv inside the folder
   ```
   virtualenv -p python3 venv
   ```
3. Create the src directory
   ```
   mkdir src
   ```
4. Clone this repo into src
   ```
   git clone git@github.com:quanticle/status_update_app.git src
   ```
5. Install prerequisites
   ```
   cd src
   pip install -r requirements.txt
   ```
6. Try launching the dev server
   ```
   cd status_update
   python manage.py runserver
   ```
   If you're developing in a VM, do
   ```
   python manage.py runserver 0.0.0.0:8000
   ```
   to make the dev server accessible from outside the VM.
